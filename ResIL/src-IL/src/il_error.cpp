/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
//-----------------------------------------------------------------------------
//
// ImageLib Sources
// Copyright (C) 2000-2008 by Denton Woods
// Last modified: 06/02/2007
//
// Filename: src-IL/src/il_error.c
//
// Description: The error functions
//
//-----------------------------------------------------------------------------

#include "../include/il_internal.h"

#define IL_ERROR_STACK_SIZE 32  // Needed elsewhere?

// Global variable: ilError stack
// Would have to be protected by a mutex in a multithreaded environment
ILenum	ilErrorNum[IL_ERROR_STACK_SIZE];
ILint	ilErrorPlace = (-1);

// Sets the current error
//	If you go past the stack size for this, it cycles the errors, almost like a LRU algo.
ILAPI void ILAPIENTRY ilSetError(ILenum Error)
{
	ILuint i;

	ilErrorPlace++;
	if (ilErrorPlace >= IL_ERROR_STACK_SIZE) {
		for (i = 0; i < IL_ERROR_STACK_SIZE - 2; i++) {
			ilErrorNum[i] = ilErrorNum[i + 1];
		}
		ilErrorPlace = IL_ERROR_STACK_SIZE - 1;
	}
	ilErrorNum[ilErrorPlace] = Error;

	return;
}

//! Gets the last error on the error stack
ILenum ILAPIENTRY ilGetError(void)
{
	ILenum ilReturn;

	if (ilErrorPlace >= 0) {
		ilReturn = ilErrorNum[ilErrorPlace];
		ilErrorPlace--;
	}
	else
		ilReturn = IL_NO_ERROR;

	return ilReturn;
}