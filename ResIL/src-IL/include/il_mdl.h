/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
//-----------------------------------------------------------------------------
//
// ImageLib Sources
// Copyright (C) 2000-2002 by Denton Woods
// Last modified: 05/25/2001 <--Y2K Compliant! =]
//
// Filename: src-IL/include/il_mdl.h
//
// Description: Reads a Half-Life model file.
//
//-----------------------------------------------------------------------------

#ifndef MD2_H
#define MD2_H

#include "il_internal.h"

typedef struct TEX_HEAD
{
	char	Name[64];
	ILuint	Flags;
	ILuint	Width;
	ILuint	Height;
	ILuint	Offset;
} TEX_HEAD;

#endif//MD2_H
