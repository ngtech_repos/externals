/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
//-----------------------------------------------------------------------------
//
// ImageLib Sources
// Copyright (C) 2000-2004 by Denton Woods (this file by thakis)
// Last modified: 06/09/2004
//
// Filename: src-IL/include/il_hdr.h
//
// Description: Reads a RADIANCE High Dynamic Range Image
//
//-----------------------------------------------------------------------------

#ifndef HDR_H
#define HDR_H

#include "il_internal.h"

#ifdef _WIN32
#pragma pack(push, gif_struct, 1)
#endif

#ifdef _WIN32
#pragma pack(pop, gif_struct)
#endif

// Internal functions
ILboolean iIsValidHdr(SIO* io);
ILboolean iLoadHdrInternal(ILimage* image);
ILboolean iSaveHdrInternal(ILimage* image);

#endif//HDR_H
