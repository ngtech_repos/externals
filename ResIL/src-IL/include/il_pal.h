/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
//-----------------------------------------------------------------------------
//
// ImageLib Sources
// Copyright (C) 2000-2002 by Denton Woods
// Last modified: 05/25/2001 <--Y2K Compliant! =]
//
// Filename: src-IL/include/il_pal.h
//
// Description: Loads palettes from different file formats
//
//-----------------------------------------------------------------------------

#ifndef IL_PAL_H
#define IL_PAL_H

#include "il_internal.h"

#define BUFFLEN	256
#define PALBPP	3

#ifdef _MSC_VER
#pragma pack(push, packed_struct, 1)
#endif
typedef struct HALOHEAD
{
	ILushort	Id;  // 'AH'
	ILshort		Version;
	ILshort		Size;
	ILbyte		Filetype;
	ILbyte		Subtype;
	//ILshort	Brdid, Grmode;
	ILint		Ignored;
	ILushort	MaxIndex;  // Colors = maxindex + 1
	ILushort	MaxRed;
	ILushort	MaxGreen;
	ILushort	MaxBlue;
	/*ILbyte	Signature[8];
	ILbyte		Filler[12];*/
	ILbyte		Filler[20];  // Always 0 by PSP 4
} IL_PACKSTRUCT HALOHEAD;
#ifdef _MSC_VER
#pragma pack(pop,  packed_struct)
#endif

ILboolean	ilLoadJascPal(ILconst_string FileName);
ILboolean	ilSaveJascPal(ILconst_string FileName);
char		*iFgetw(ILubyte *Buff, ILint MaxLen, FILE *File);
ILboolean	ilLoadHaloPal(ILconst_string FileName);
ILboolean	ilLoadColPal(ILconst_string FileName);
ILboolean	ilLoadActPal(ILconst_string FileName);
ILboolean	ilLoadPltPal(ILconst_string FileName);

#endif//IL_PAL_H
