/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
//-----------------------------------------------------------------------------
//
// ImageLib Sources
// Copyright (C) 2000-2009 by Denton Woods
// Last modified: 01/04/2009
//
// Filename: src-IL/include/il_files.h
//
// Description: File handling for DevIL
//
//-----------------------------------------------------------------------------

#ifndef FILES_H
#define FILES_H

#if defined (__FILES_C)
#define __FILES_EXTERN
#else
#define __FILES_EXTERN extern
#endif
#include "../../include/IL/il.h"

// Declaration of default read/write functions, used internally e.g. for ilLoadImage
__FILES_EXTERN ILHANDLE			ILAPIENTRY iDefaultOpen(ILconst_string FileName);
__FILES_EXTERN void		        ILAPIENTRY iDefaultClose(ILHANDLE Handle);
__FILES_EXTERN ILint			ILAPIENTRY iDefaultGetc(ILHANDLE Handle);
__FILES_EXTERN ILuint			ILAPIENTRY iDefaultRead(void *Buffer, ILuint Size, ILuint Number, ILHANDLE Handle);
__FILES_EXTERN ILint			ILAPIENTRY iDefaultSeekR(ILHANDLE Handle, ILint64 Offset, ILint Mode);
__FILES_EXTERN ILint			ILAPIENTRY iDefaultSeekW(ILHANDLE Handle, ILint Offset, ILint Mode);
__FILES_EXTERN ILint			ILAPIENTRY iDefaultTellR(ILHANDLE Handle);
__FILES_EXTERN ILint			ILAPIENTRY iDefaultTellW(ILHANDLE Handle);
__FILES_EXTERN ILint			ILAPIENTRY iDefaultPutc(ILubyte Char, ILHANDLE Handle);
__FILES_EXTERN ILint			ILAPIENTRY iDefaultWrite(const void *Buffer, ILuint Size, ILuint Number, ILHANDLE Handle);

// Functions to set file or lump for reading/writing
__FILES_EXTERN void				iSetInputFile(ILHANDLE File);
__FILES_EXTERN void				iSetInputLump(const void *Lump, ILuint Size);
__FILES_EXTERN void				iSetOutputFile(ILHANDLE File);
__FILES_EXTERN void				iSetOutputLump(void *Lump, ILuint Size);
__FILES_EXTERN void				iSetOutputFake(void);

__FILES_EXTERN ILHANDLE			ILAPIENTRY iGetFile(void);
__FILES_EXTERN const ILubyte*	ILAPIENTRY iGetLump(void);

__FILES_EXTERN ILuint			ILAPIENTRY ilprintf(const char *, ...);
__FILES_EXTERN void				ipad(ILuint NumZeros);

#endif//FILES_H
