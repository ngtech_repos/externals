/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
//-----------------------------------------------------------------------------
//
// ImageLib Sources
// Copyright (C) 2000-2002 by Denton Woods
// Last modified: 05/25/2001 <--Y2K Compliant! =]
//
// Filename: src-IL/include/il_bits.h
//
// Description: Implements a file class that reads/writes bits directly.
//
//-----------------------------------------------------------------------------

#ifndef BITS_H
#define BITS_H

#include "il_internal.h"

// Struct for dealing with reading bits from a file
typedef struct BITFILE
{
	ILHANDLE	File;
	ILuint		BitPos;
	ILint		ByteBitOff;
	ILubyte		Buff;
} BITFILE;

// Functions for reading bits from a file
//BITFILE*	bopen(const char *FileName, const char *Mode);
ILint		bclose(BITFILE *BitFile);
BITFILE*	bfile(ILHANDLE File);
ILint		btell(BITFILE *BitFile);
ILint		bseek(BITFILE *BitFile, ILuint Offset, ILuint Mode);
ILint		bread(void *Buffer, ILuint Size, ILuint Number, BITFILE *BitFile);
//ILint		bwrite(void *Buffer, ILuint Size, ILuint Number, BITFILE *BitFile);

// Useful macros for manipulating bits
#define SetBits(var, bits)		(var |= bits)
#define ClearBits(var, bits)	(var &= ~(bits))

#endif//BITS_H
