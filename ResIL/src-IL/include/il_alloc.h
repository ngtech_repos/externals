/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#ifndef ALLOC_H
#define ALLOC_H

/*#if defined(_WIN32) && defined(_MEM_DEBUG)
	void *c_alloc(unsigned long size, unsigned long num, const char *file, unsigned long line);
	void *m_alloc(unsigned long size, const char *file, unsigned long line);
	void f_ree(void *ptr);

	#ifdef malloc
	#undef malloc
	#endif

	#ifdef calloc
	#undef calloc
	#endif

	#ifdef free
	#undef free
	#endif

	#define malloc(size) m_alloc(size, __FILE__, __LINE__)
	#define calloc(size, num) c_alloc(size, num, __FILE__, __LINE__)
	#define free(addr) f_ree(addr)
#endif//defined(_WIN32) && defined(_MEM_DEBUG)*/

#if defined (__ALLOC_C)
#define __ALLOC_EXTERN
#else
#define __ALLOC_EXTERN extern
#endif
#include <IL/il.h>

__ALLOC_EXTERN mAlloc ialloc_ptr;
__ALLOC_EXTERN mFree  ifree_ptr;

#endif//ALLOC_H
