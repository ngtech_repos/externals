/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
//-----------------------------------------------------------------------------
//
// ImageLib Sources
// Copyright (C) 2000-2002 by Denton Woods
// Last modified: 05/25/2001 <--Y2K Compliant! =]
//
// Filename: src-IL/include/il_pnm.h
//
// Description: Reads/writes to/from pbm/pgm/ppm formats
//
//-----------------------------------------------------------------------------

#ifndef PPMPGM_H
#define PPMPGM_H

#include "il_internal.h"

#define IL_PBM_ASCII	0x0001
#define IL_PGM_ASCII	0x0002
#define IL_PPM_ASCII	0x0003
#define IL_PBM_BINARY	0x0004
#define IL_PGM_BINARY	0x0005
#define IL_PPM_BINARY	0x0006

typedef struct PPMINFO
{
	ILenum	Type;
	ILuint	Width;
	ILuint	Height;
	ILuint	MaxColour;
	ILubyte	Bpp;
} PPMINFO;

ILboolean	iCheckPnm(char Header[2]);
ILimage		*ilReadAsciiPpm(PPMINFO *Info);
ILimage		*ilReadBinaryPpm(PPMINFO *Info);
ILimage		*ilReadBitPbm(PPMINFO *Info);
ILboolean	iGetWord(ILboolean);
void		PbmMaximize(ILimage *Image);

#endif//PPMPGM_H
