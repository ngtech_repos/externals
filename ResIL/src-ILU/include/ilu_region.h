/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
//-----------------------------------------------------------------------------
//
// ImageLib Utility Sources
// Copyright (C) 2000-2002 by Denton Woods
// Last modified: 07/09/2002 <--Y2K Compliant! =]
//
// Filename: src-ILU/src/ilu_region.h
//
// Description: Creates an image region.
//
//-----------------------------------------------------------------------------

#ifndef ILU_REGION_H
#define ILU_REGION_H

#include "ilu_internal.h"

typedef struct Edge
{
	ILint	yUpper;
	ILfloat	xIntersect, dxPerScan;
	struct	Edge *next;
} Edge;

#endif//ILU_REGION_H