/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
//-----------------------------------------------------------------------------
//
// ImageLib Utility Sources
// Copyright (C) 2000-2009 by Denton Woods
// Last modified: 02/21/2009
//
// Filename: src-ILU/src/ilu_mipmap.c
//
// Description: Generates mipmaps for the current image.
//
//-----------------------------------------------------------------------------

#include "../include/ilu_internal.h"

ILboolean iBuildMipmaps(ILimage *Parent, ILuint Width, ILuint Height, ILuint Depth)
{
	ILuint	x1 = 0, x2 = 0, y1 = 0, y2 = 0;

	if (Parent->Width == 1 && Parent->Height == 1 && Parent->Depth == 1) {  // Already at the last mipmap
		return IL_TRUE;
	}

	if (Width == 0)
		Width = 1;
	if (Height == 0)
		Height = 1;
	if (Depth == 0)
		Depth = 1;

	Parent->Mipmaps = iluScale_(Parent, Width, Height, Depth);
	if (Parent->Mipmaps == NULL)
		return IL_FALSE;

	iBuildMipmaps(Parent->Mipmaps, Parent->Mipmaps->Width >> 1, Parent->Mipmaps->Height >> 1, Parent->Mipmaps->Depth >> 1);

	return IL_TRUE;
}

// Note: No longer changes all textures to powers of 2.
ILboolean ILAPIENTRY iluBuildMipmaps()
{
	iluCurImage = ilGetCurImage();
	if (iluCurImage == NULL) {
		ilSetError(ILU_ILLEGAL_OPERATION);
		return IL_FALSE;
	}

	// Get rid of any existing mipmaps.
	if (iluCurImage->Mipmaps) {
		ilCloseImage(iluCurImage->Mipmaps);
		iluCurImage->Mipmaps = NULL;
	}

	return iBuildMipmaps(iluCurImage, iluCurImage->Width >> 1, iluCurImage->Height >> 1, iluCurImage->Depth >> 1);
}