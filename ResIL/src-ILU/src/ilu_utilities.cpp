/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
//-----------------------------------------------------------------------------
//
// ImageLib Utility Sources
// Copyright (C) 2000-2002 by Denton Woods
// Last modified: 05/25/2001 <--Y2K Compliant! =]
//
// Filename: src-ILU/src/ilu_utilities.c
//
// Description: Utility functions
//
//-----------------------------------------------------------------------------

#include "../include/ilu_internal.h"

void ILAPIENTRY iluDeleteImage(ILuint Id)
{
	ilDeleteImages(1, &Id);
	return;
}

ILuint ILAPIENTRY iluGenImage()
{
	ILuint Id;
	ilGenImages(1, &Id);
	ilBindImage(Id);
	return Id;
}

//! Retrieves information about the current bound image.
void ILAPIENTRY iluGetImageInfo(ILinfo *Info)
{
	iluCurImage = ilGetCurImage();
	if (iluCurImage == NULL || Info == NULL) {
		ilSetError(ILU_ILLEGAL_OPERATION);
		return;
	}

	Info->Id = ilGetCurName();
	Info->Data = ilGetData();
	Info->Width = iluCurImage->Width;
	Info->Height = iluCurImage->Height;
	Info->Depth = iluCurImage->Depth;
	Info->Bpp = iluCurImage->Bpp;
	Info->SizeOfData = iluCurImage->SizeOfData;
	Info->Format = iluCurImage->Format;
	Info->Type = iluCurImage->Type;
	Info->Origin = iluCurImage->Origin;
	Info->Palette = iluCurImage->Pal.Palette;
	Info->PalType = iluCurImage->Pal.PalType;
	Info->PalSize = iluCurImage->Pal.PalSize;
	iGetIntegervImage(iluCurImage, IL_NUM_IMAGES,
		(ILint*)&Info->NumNext);
	iGetIntegervImage(iluCurImage, IL_NUM_MIPMAPS,
		(ILint*)&Info->NumMips);
	iGetIntegervImage(iluCurImage, IL_NUM_LAYERS,
		(ILint*)&Info->NumLayers);

	return;
}