/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
//-----------------------------------------------------------------------------
//
// ImageLib Utility Sources
// Copyright (C) 2000-2009 by Denton Woods
// Last modified: 03/07/2009
//
// Filename: src-ILU/src/ilu_states.c
//
// Description: The state machine
//
//-----------------------------------------------------------------------------

#include "../include/ilu_internal.h"
#include "../include/ilu_states.h"

ILconst_string _iluVendor = IL_TEXT("Abysmal Software");
ILconst_string _iluVersion = IL_TEXT("Developer's Image Library Utilities (ILU) 1.7.8");// IL_TEXT(__DATE__));

ILstring ILAPIENTRY iluGetString(ILenum StringName)
{
	switch (StringName)
	{
	case ILU_VENDOR:
		return (ILstring)_iluVendor;
		//changed 2003-09-04
	case ILU_VERSION_NUM:
		return (ILstring)_iluVersion;
	default:
		ilSetError(ILU_INVALID_PARAM);
		break;
	}
	return NULL;
}

void ILAPIENTRY iluGetIntegerv(ILenum Mode, ILint *Param)
{
	switch (Mode)
	{
	case ILU_VERSION_NUM:
		*Param = ILU_VERSION;
		break;

	case ILU_FILTER:
		*Param = iluFilter;
		break;

	default:
		ilSetError(ILU_INVALID_ENUM);
	}
	return;
}

ILint ILAPIENTRY iluGetInteger(ILenum Mode)
{
	ILint Temp;
	Temp = 0;
	iluGetIntegerv(Mode, &Temp);
	return Temp;
}

ILenum iluFilter = ILU_NEAREST;
ILenum iluPlacement = ILU_CENTER;

void ILAPIENTRY iluImageParameter(ILenum PName, ILenum Param)
{
	switch (PName)
	{
	case ILU_FILTER:
		switch (Param)
		{
		case ILU_NEAREST:
		case ILU_LINEAR:
		case ILU_BILINEAR:
		case ILU_SCALE_BOX:
		case ILU_SCALE_TRIANGLE:
		case ILU_SCALE_BELL:
		case ILU_SCALE_BSPLINE:
		case ILU_SCALE_LANCZOS3:
		case ILU_SCALE_MITCHELL:
			iluFilter = Param;
			break;
		default:
			ilSetError(ILU_INVALID_ENUM);
			return;
		}
		break;

	case ILU_PLACEMENT:
		switch (Param)
		{
		case ILU_LOWER_LEFT:
		case ILU_LOWER_RIGHT:
		case ILU_UPPER_LEFT:
		case ILU_UPPER_RIGHT:
		case ILU_CENTER:
			iluPlacement = Param;
			break;
		default:
			ilSetError(ILU_INVALID_ENUM);
			return;
		}
		break;

	default:
		ilSetError(ILU_INVALID_ENUM);
		return;
	}
	return;
}