/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
/* gzclose.c -- zlib gzclose() function
 * Copyright (C) 2004, 2010 Mark Adler
 * For conditions of distribution and use, see copyright notice in zlib.h
 */

#include "gzguts.h"

 /* gzclose() is in a separate file so that it is linked in only if it is used.
	That way the other gzclose functions can be used instead to avoid linking in
	unneeded compression or decompression routines. */
int ZEXPORT gzclose(file)
gzFile file;
{
#ifndef NO_GZCOMPRESS
	gz_statep state;

	if (file == NULL)
		return Z_STREAM_ERROR;
	state = (gz_statep)file;

	return state->mode == GZ_READ ? gzclose_r(file) : gzclose_w(file);
#else
	return gzclose_r(file);
#endif
}