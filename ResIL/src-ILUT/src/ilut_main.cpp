/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
//-----------------------------------------------------------------------------
//
// ImageLib Utility Toolkit Sources
// Copyright (C) 2000-2002 by Denton Woods
// Last modified: 05/28/2001 <--Y2K Compliant! =]
//
// Filename: src-ILUT/src/ilut_main.c
//
// Description: Startup functions
//
//-----------------------------------------------------------------------------

#include "../include/ilut_internal.h"

#ifdef _WIN32
#ifndef IL_STATIC_LIB
	//#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#ifdef _WIN32
#if (defined(IL_USE_PRAGMA_LIBS))
#if defined(_MSC_VER) || defined(__BORLANDC__)
#pragma comment(lib, "ILU.lib")
#endif
#endif
#endif

BOOL APIENTRY DllMain(HANDLE hModule, DWORD ul_reason_for_call, LPVOID lpReserved)
{
	hModule;  lpReserved;

	// only initialize when attached to a new process. setup can cause errors in OpenIL
	// when called on a per thread basis
	if (ul_reason_for_call == DLL_PROCESS_ATTACH) {
		//ilutInit();
	}

	return TRUE;
}
#endif

#else  // Should check if gcc?

// Should be able to condense this...
static void GccMain() __attribute__((constructor));
static void GccMain()
{
	//ilutInit();
}

#endif

void ILAPIENTRY ilutInit()
{
	ilutDefaultStates();  // Set states to their defaults
	// Can cause crashes if DevIL is not initialized yet

#ifdef ILUT_USE_OPENGL
	ilutGLInit();  // default renderer is OpenGL
#endif

#ifdef ILUT_USE_DIRECTX8
	ilutD3D8Init();
#endif

#ifdef ILUT_USE_DIRECTX9
	ilutD3D9Init();
#endif

	return;
}