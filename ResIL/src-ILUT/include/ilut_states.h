/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
//-----------------------------------------------------------------------------
//
// ImageLib Utility Toolkit Sources
// Copyright (C) 2000-2002 by Denton Woods
// Last modified: 05/28/2001 <--Y2K Compliant! =]
//
// Filename: src-ILUT/include/ilut_states.h
//
// Description: State machine
//
//-----------------------------------------------------------------------------

#ifndef STATES_H
#define STATES_H

#include "ilut_internal.h"

ILboolean ilutAble(ILenum Mode, ILboolean Flag);

#define ILUT_ATTRIB_STACK_MAX 32

ILuint ilutCurrentPos = 0;  // Which position on the stack

//
// Various states
//

typedef struct ILUT_STATES
{
	// ILUT states
	ILboolean	ilutUsePalettes;
	ILboolean	ilutOglConv;
	ILboolean	ilutForceIntegerFormat;
	ILenum		ilutDXTCFormat;

	// GL states
	ILboolean	ilutUseS3TC;
	ILboolean	ilutGenS3TC;
	ILboolean	ilutAutodetectTextureTarget;
	ILint		MaxTexW;
	ILint		MaxTexH;
	ILint		MaxTexD;

	// D3D states
	ILuint		D3DMipLevels;
	ILenum		D3DPool;
	ILint		D3DAlphaKeyColor; // 0x00rrggbb format , -1 for none
} ILUT_STATES;

ILUT_STATES ilutStates[ILUT_ATTRIB_STACK_MAX];

#endif//STATES_H