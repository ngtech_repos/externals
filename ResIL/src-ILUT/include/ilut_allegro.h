/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
//-----------------------------------------------------------------------------
//
// ImageLib Utility Toolkit Sources
// Copyright (C) 2000-2002 by Denton Woods
// Last modified: 04/20/2002 <--Y2K Compliant! =]
//
// Filename: src-ILUT/include/ilut_alleg.h (Don't want to conflict with allegro.h)
//
// Description:
//
//-----------------------------------------------------------------------------

// xxxxAllegro.h has to be included before il/il.h!xxx
#ifdef ILUT_USE_ALLEGRO

#ifndef ILUT_ALLEG_H
#define ILUT_ALLEG_H

#include <allegro.h>

#endif//ILUT_ALLEG_H
#endif//ILUT_USE_ALLEGRO