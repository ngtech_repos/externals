/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#ifndef AL_BOOL_H
#define AL_BOOL_H

#ifdef HAVE_STDBOOL_H
#include <stdbool.h>
#endif

#ifndef bool
#ifdef HAVE_C99_BOOL
#define bool _Bool
#else
#define bool int
#endif
#define false 0
#define true 1
#endif

#endif /* AL_BOOL_H */
