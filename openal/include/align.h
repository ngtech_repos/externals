/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#ifndef AL_ALIGN_H
#define AL_ALIGN_H

#if defined(HAVE_STDALIGN_H) && defined(HAVE_C11_ALIGNAS)
#include <stdalign.h>
#endif

#ifndef alignas
#if defined(IN_IDE_PARSER)
/* KDevelop has problems with our align macro, so just use nothing for parsing. */
#define alignas(x)
#elif defined(HAVE_C11_ALIGNAS)
#define alignas _Alignas
#else
/* NOTE: Our custom ALIGN macro can't take a type name like alignas can. For
 * maximum compatibility, only provide constant integer values to alignas. */
#define alignas(_x) ALIGN(_x)
#endif
#endif

#endif /* AL_ALIGN_H */
