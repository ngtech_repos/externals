/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#ifndef AL_STATIC_ASSERT_H
#define AL_STATIC_ASSERT_H

#include <assert.h>

#ifndef static_assert
#ifdef HAVE_C11_STATIC_ASSERT
#define static_assert _Static_assert
#else
#define CTASTR2(_pre,_post) _pre##_post
#define CTASTR(_pre,_post) CTASTR2(_pre,_post)
#if defined(__COUNTER__)
#define static_assert(_cond, _msg) typedef struct { int CTASTR(static_assert_failed_at_line_,__LINE__) : !!(_cond); } CTASTR(static_assertion_,__COUNTER__)
#else
#define static_assert(_cond, _msg) struct { int CTASTR(static_assert_failed_at_line_,__LINE__) : !!(_cond); }
#endif
#endif
#endif

#endif /* AL_STATIC_ASSERT_H */
