/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#ifndef _AL_LISTENER_H_
#define _AL_LISTENER_H_

#include "alMain.h"
#include "alu.h"

#ifdef __cplusplus
extern "C" {
#endif

	typedef struct ALlistener {
		aluVector Position;
		aluVector Velocity;
		volatile ALfloat Forward[3];
		volatile ALfloat Up[3];
		volatile ALfloat Gain;
		volatile ALfloat MetersPerUnit;

		struct {
			aluMatrixd Matrix;
			aluVector  Velocity;
		} Params;
	} ALlistener;

#ifdef __cplusplus
}
#endif

#endif
