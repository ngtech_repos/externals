/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "openal_config.h"

#include <signal.h>

#ifdef HAVE_WINDOWS_H
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif

#include "alMain.h"
#include "AL/alc.h"
#include "alError.h"

ALboolean TrapALError = AL_FALSE;

ALvoid alSetError(ALCcontext *Context, ALenum errorCode)
{
	ALenum curerr = AL_NO_ERROR;
	if (TrapALError)
	{
#ifdef _WIN32
		/* DebugBreak will cause an exception if there is no debugger */
		if (IsDebuggerPresent())
			DebugBreak();
#elif defined(SIGTRAP)
		raise(SIGTRAP);
#endif
	}
	ATOMIC_COMPARE_EXCHANGE_STRONG(ALenum, &Context->LastError, &curerr, errorCode);
}

AL_API ALenum AL_APIENTRY alGetError(void)
{
	ALCcontext *Context;
	ALenum errorCode;

	Context = GetContextRef();
	if (!Context)
	{
		if (TrapALError)
		{
#ifdef _WIN32
			if (IsDebuggerPresent())
				DebugBreak();
#elif defined(SIGTRAP)
			raise(SIGTRAP);
#endif
		}
		return AL_INVALID_OPERATION;
	}

	errorCode = ATOMIC_EXCHANGE(ALenum, &Context->LastError, AL_NO_ERROR);

	ALCcontext_DecRef(Context);

	return errorCode;
}