#ifdef __clang__

#include "clang_additions.h"

#elif defined(__GNUC__) || defined(__GNUG__)
#include "gcc_additions.h"

#endif