#ifdef __gcc__
// NickGalko:Disabled warning for building under clang,because
// in gcc exist bug with SSE
#pragma gcc diagnostic ignored "-Wreturn-type-c-linkage"
#pragma gcc diagnostic ignored "-Wabsolute-value"
#pragma gcc diagnostic ignored "-Wundefined-bool-conversion"
#pragma gcc diagnostic ignored "-Wunused-private-field"
#pragma gcc diagnostic ignored "-Wnon-pod-varargs"
#endif