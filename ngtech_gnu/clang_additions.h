#ifdef __clang__
// NickGalko:Disabled warning for building under clang,because
// in gcc exist bug with SSE
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
#pragma clang diagnostic ignored "-Wabsolute-value"
#pragma clang diagnostic ignored "-Wundefined-bool-conversion"
#pragma clang diagnostic ignored "-Wunused-private-field"
#pragma clang diagnostic ignored "-Wnon-pod-varargs"
#endif