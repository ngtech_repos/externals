#if defined(__LINUX__) && (!defined(__ANDROID_API__) ) || defined( LINUX )|| defined( ANDROID )
#include "openal_config_android.h"
#elif  defined(WIN32) || defined(_MSC_VER)
#include "openal_config_windows.h"
#else
#pragma error("NOT DETECTED PLATFORM FOR OPENAL")
#endif