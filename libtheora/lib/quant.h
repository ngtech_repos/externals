/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
/********************************************************************
 *                                                                  *
 * THIS FILE IS PART OF THE OggTheora SOFTWARE CODEC SOURCE CODE.   *
 * USE, DISTRIBUTION AND REPRODUCTION OF THIS LIBRARY SOURCE IS     *
 * GOVERNED BY A BSD-STYLE SOURCE LICENSE INCLUDED WITH THIS SOURCE *
 * IN 'COPYING'. PLEASE READ THESE TERMS BEFORE DISTRIBUTING.       *
 *                                                                  *
 * THE Theora SOURCE CODE IS COPYRIGHT (C) 2002-2009                *
 * by the Xiph.Org Foundation and contributors http://www.xiph.org/ *
 *                                                                  *
 ********************************************************************

  function:
	last mod: $Id: quant.h 16503 2009-08-22 18:14:02Z giles $

 ********************************************************************/

#if !defined(_quant_H)
# define _quant_H (1)

# include "../include/theora/codec.h"

# include "ocintrin.h"

typedef ogg_uint16_t   oc_quant_table[64];

/*Maximum scaled quantizer value.*/
#define OC_QUANT_MAX          (1024<<2)

void oc_dequant_tables_init(ogg_uint16_t *_dequant[64][3][2],
	int _pp_dc_scale[64], const th_quant_info *_qinfo);

#endif
