/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#if !defined(_huffenc_H)
# define _huffenc_H (1)
# include "huffman.h"

typedef th_huff_code                  th_huff_table[TH_NDCT_TOKENS];

extern const th_huff_code
TH_VP31_HUFF_CODES[TH_NHUFFMAN_TABLES][TH_NDCT_TOKENS];

int oc_huff_codes_pack(oggpack_buffer *_opb,
	const th_huff_code _codes[TH_NHUFFMAN_TABLES][TH_NDCT_TOKENS]);

#endif
