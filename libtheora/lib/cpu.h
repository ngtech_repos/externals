/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
/********************************************************************
 *                                                                  *
 * THIS FILE IS PART OF THE OggTheora SOFTWARE CODEC SOURCE CODE.   *
 * USE, DISTRIBUTION AND REPRODUCTION OF THIS LIBRARY SOURCE IS     *
 * GOVERNED BY A BSD-STYLE SOURCE LICENSE INCLUDED WITH THIS SOURCE *
 * IN 'COPYING'. PLEASE READ THESE TERMS BEFORE DISTRIBUTING.       *
 *                                                                  *
 * THE Theora SOURCE CODE IS COPYRIGHT (C) 2002-2009                *
 * by the Xiph.Org Foundation and contributors http://www.xiph.org/ *
 *                                                                  *
 ********************************************************************
 function:
	last mod: $Id: cpu.h 16503 2009-08-22 18:14:02Z giles $

 ********************************************************************/

#if !defined(_x86_cpu_H)
# define _x86_cpu_H (1)
#include "internal.h"

#define OC_CPU_X86_MMX      (1<<0)
#define OC_CPU_X86_3DNOW    (1<<1)
#define OC_CPU_X86_3DNOWEXT (1<<2)
#define OC_CPU_X86_MMXEXT   (1<<3)
#define OC_CPU_X86_SSE      (1<<4)
#define OC_CPU_X86_SSE2     (1<<5)
#define OC_CPU_X86_PNI      (1<<6)
#define OC_CPU_X86_SSSE3    (1<<7)
#define OC_CPU_X86_SSE4_1   (1<<8)
#define OC_CPU_X86_SSE4_2   (1<<9)
#define OC_CPU_X86_SSE4A    (1<<10)
#define OC_CPU_X86_SSE5     (1<<11)

#endif
