/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
/********************************************************************
 *                                                                  *
 * THIS FILE IS PART OF THE OggTheora SOFTWARE CODEC SOURCE CODE.   *
 * USE, DISTRIBUTION AND REPRODUCTION OF THIS LIBRARY SOURCE IS     *
 * GOVERNED BY A BSD-STYLE SOURCE LICENSE INCLUDED WITH THIS SOURCE *
 * IN 'COPYING'. PLEASE READ THESE TERMS BEFORE DISTRIBUTING.       *
 *                                                                  *
 * THE Theora SOURCE CODE IS COPYRIGHT (C) 2002-2009                *
 * by the Xiph.Org Foundation and contributors http://www.xiph.org/ *
 *                                                                  *
 ********************************************************************

  function:
	last mod: $Id: x86int.h 15675 2009-02-06 09:43:27Z tterribe $

 ********************************************************************/

#if !defined(_x86_vc_x86enc_H)
# define _x86_vc_x86enc_H (1)
# include "../encint.h"
# include "x86int.h"

void oc_enc_vtable_init_x86(oc_enc_ctx *_enc);

unsigned oc_enc_frag_sad_mmxext(const unsigned char *_src,
	const unsigned char *_ref, int _ystride);
unsigned oc_enc_frag_sad_thresh_mmxext(const unsigned char *_src,
	const unsigned char *_ref, int _ystride, unsigned _thresh);
unsigned oc_enc_frag_sad2_thresh_mmxext(const unsigned char *_src,
	const unsigned char *_ref1, const unsigned char *_ref2, int _ystride,
	unsigned _thresh);
unsigned oc_enc_frag_satd_thresh_mmxext(const unsigned char *_src,
	const unsigned char *_ref, int _ystride, unsigned _thresh);
unsigned oc_enc_frag_satd2_thresh_mmxext(const unsigned char *_src,
	const unsigned char *_ref1, const unsigned char *_ref2, int _ystride,
	unsigned _thresh);
unsigned oc_enc_frag_intra_satd_mmxext(const unsigned char *_src, int _ystride);
void oc_enc_frag_sub_mmx(ogg_int16_t _diff[64],
	const unsigned char *_x, const unsigned char *_y, int _stride);
void oc_enc_frag_sub_128_mmx(ogg_int16_t _diff[64],
	const unsigned char *_x, int _stride);
void oc_enc_frag_copy2_mmxext(unsigned char *_dst,
	const unsigned char *_src1, const unsigned char *_src2, int _ystride);
void oc_enc_fdct8x8_mmx(ogg_int16_t _y[64], const ogg_int16_t _x[64]);
void oc_enc_fdct8x8_x86_64sse2(ogg_int16_t _y[64], const ogg_int16_t _x[64]);

#endif
