/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
/********************************************************************
 *                                                                  *
 * THIS FILE IS PART OF THE OggTheora SOFTWARE CODEC SOURCE CODE.   *
 * USE, DISTRIBUTION AND REPRODUCTION OF THIS LIBRARY SOURCE IS     *
 * GOVERNED BY A BSD-STYLE SOURCE LICENSE INCLUDED WITH THIS SOURCE *
 * IN 'COPYING'. PLEASE READ THESE TERMS BEFORE DISTRIBUTING.       *
 *                                                                  *
 * THE Theora SOURCE CODE IS COPYRIGHT (C) 2002-2009                *
 * by the Xiph.Org Foundation and contributors http://www.xiph.org/ *
 *                                                                  *
 ********************************************************************

  function:
	last mod: $Id: x86int.h 16503 2009-08-22 18:14:02Z giles $

 ********************************************************************/

#if !defined(_x86_vc_x86int_H)
# define _x86_vc_x86int_H (1)
# include "../internal.h"

void oc_state_vtable_init_x86(oc_theora_state *_state);

void oc_frag_copy_mmx(unsigned char *_dst,
	const unsigned char *_src, int _ystride);
void oc_frag_recon_intra_mmx(unsigned char *_dst, int _ystride,
	const ogg_int16_t *_residue);
void oc_frag_recon_inter_mmx(unsigned char *_dst,
	const unsigned char *_src, int _ystride, const ogg_int16_t *_residue);
void oc_frag_recon_inter2_mmx(unsigned char *_dst, const unsigned char *_src1,
	const unsigned char *_src2, int _ystride, const ogg_int16_t *_residue);
void oc_idct8x8_mmx(ogg_int16_t _y[64], int _last_zzi);
void oc_state_frag_recon_mmx(const oc_theora_state *_state, ptrdiff_t _fragi,
	int _pli, ogg_int16_t _dct_coeffs[64], int _last_zzi, ogg_uint16_t _dc_quant);
void oc_state_frag_copy_list_mmx(const oc_theora_state *_state,
	const ptrdiff_t *_fragis, ptrdiff_t _nfragis,
	int _dst_frame, int _src_frame, int _pli);
void oc_state_loop_filter_frag_rows_mmx(const oc_theora_state *_state,
	int _bv[256], int _refi, int _pli, int _fragy0, int _fragy_end);
void oc_restore_fpu_mmx(void);

#endif
